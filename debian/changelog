libcanary-stability-perl (2013-1) unstable; urgency=medium

  * Team upload.
  * debian/watch: use uscan version 4 and uscan macros.
  * Import upstream version 2013.
  * Switch from cdbs to debhelper.
  * Switch to "Architecture: all". (Closes: #809396)
  * Declare compliance with Debian Policy 4.7.0.
  * Set Rules-Requires-Root: no.

 -- gregor herrmann <gregoa@debian.org>  Sat, 27 Apr 2024 22:07:21 +0200

libcanary-stability-perl (2006-5) unstable; urgency=medium

  * Team upload.
  * Remove Makefile.old. (Closes: #1047389)

 -- gregor herrmann <gregoa@debian.org>  Mon, 04 Mar 2024 21:42:15 +0100

libcanary-stability-perl (2006-4) unstable; urgency=medium

  * Team upload.
  * Fix autopkgtests: skip use.t.

 -- gregor herrmann <gregoa@debian.org>  Thu, 16 Jun 2022 16:35:21 +0200

libcanary-stability-perl (2006-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Bump debhelper dependency to >= 9, since that's what is used in
    debian/compat.
  * Bump debhelper from deprecated 9 to 10.
  * Apply multi-arch hints.
    + libcanary-stability-perl: Add Multi-Arch: same.
  * Remove 1 unused lintian overrides.
  * Set Testsuite header for perl package.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 09 Jun 2022 21:33:01 +0100

libcanary-stability-perl (2006-2) unstable; urgency=medium

  * Stop check license during build:
    + Stop include cdbs snippet utils.mk.
    + Stop build-depend on devscripts.
    Closes: Bug#928695. Thanks to Helmut Grohne.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 11 May 2019 11:39:53 +0200

libcanary-stability-perl (2006-1) unstable; urgency=low

  * Initial release.
    Closes: Bug#794672.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 05 Aug 2015 17:40:20 +0200
